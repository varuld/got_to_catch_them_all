# Assignment 4 - pokemon catcher


## Table of Contents

- [Requirements](#requirements)
- [Setup](#setup)
- [Scripts](#scripts)
- [Automatization](#automatization)
- [Program Description](#program-description)
- [Members' tasks](#members-tasks)
- [Maintainers](#maintainers)
- [Contributers](#contributers)

## Requirements

Requires `gcc`, `cmake`, `nlohmann_json`, `restclient-cpp` and `pip3`.

---

## Setup
	$ run the automation script 'auto_script' by;
	$ ./auto_script
	$ ATTENTION: this will write to and late delete your crontab

Alternatively; - (Requires -  the Python Generic DB Server (PGRS): https://gitlab.com/noroff-accelerate/embedded-bootcamp/projects/python-generic-db-rest-server)

    $ Start the PGRS as prescribed in their README and let it run.
    $ mkdir build
    $ cd build
    $ cmake ../
    $ make
    $ ./main

---


## Scripts

## Automatization
The 'auto_script' is able to setup the flask server, compile and execute
the program, as well as setting up the cronjobs required and shutting down the processes when complete

## Clean database

To clean the database file run 'clean_DB'

---

## Program Description
The main.cpp program has the following structure:
* Check if the Python REST server is running, if so:
* Retrieve all data from the server
* Create a list of all the pokemons stored in the database
* If less than 898 pokemons are in the database, add another one by:
* Fetching one from the pokeapi and checking if it already exists in the database
* If it does, try again until a new one is found.

---

## Members' tasks
This list describes the tasks completed by each member in the group.

**[Thorbjørn (@Thunderl3ear)](https://gitlab.com/Thunderl3ear):**

* Setting up the CMakeList
* Local server interactions
* Pokeapi interactions
* main function
* Wrote in this readme

**[Christian (@varuld)](https://gitlab.com/varuld):**

* Automation script
* Auto server setup
* Cronjobs
* Merging


## Maintainers

[Christian (@varuld)](https://gitlab.com/varuld)

## Contributors and License
Copyright 2022,

[Thorbjørn (@Thunderl3ear)](https://gitlab.com/Thunderl3ear)
[Christian (@varuld)](https://gitlab.com/varuld)
