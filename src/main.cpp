#include <iostream>
#include <random>
#include "pokem.hpp"

// Setting up a random seeded uniform distribution.
std::random_device randomDevice;
std::mt19937 randomEngine(randomDevice());
std::uniform_int_distribution<int> uniformDistribution(1,898);
// Returns a member of the unifrom distribution [1,898].
int get_random_integer()
{
    return uniformDistribution(randomEngine);
}

int main(int, char**) {
    std::cout << "Getting list of pokemons" << std::endl;


    restIO client = restIO("http://127.0.0.1:5000");
    if(client.is_connected()) // handles error in pokemon.cpp
    {
		// get poke data from local database(already cought)
        std::string myData = client.get_all_data();

        // Convert string to json
        auto json_body = json::parse(myData);

        std::cout << "Pokemons stored in the database:" << std::endl;

        // List for storing the found pokemons
        std::vector<std::string> poke_names;
        // Number of elements on the server.
        int N = json_body.size();
        for(int i=0;i<N;i++)
        {
            // Only look through the valid data entries.
            if(json_body[i]["meta_data"]=="pokemon")
            {
				// parse and add to vector
                std::string test = json_body[i]["data"];
                auto json_data = json::parse(test);
                poke_names.push_back(json_data["name"]);
                std::cout << json_data["name"] << std::endl;
            }
        }
        // Check if the database contains all pokemon
        if(poke_names.size()<898)
        {
            std::cout << "Fetching a new pokemon" << std::endl;

            bool a_new_poke = false;
            pokemon api_poke;
            while(!a_new_poke)
            {
				// only write priviously non-cought pokemon
                int myInt = get_random_integer();
                pokeapi pokeIO = pokeapi("https://pokeapi.co/api/v2/pokemon/");
                std::string string_poke = pokeIO.get_pokemon(myInt);
                api_poke = pokeIO.create_pokemon(string_poke);
                a_new_poke = poke_found(poke_names, api_poke.name);
            }
            client.write_entry(api_poke);
        }
        else
        {
            std::cout << "You found all the pokemons" << std::endl;
            write_txt("/tmp/pokedex_complete.txt", "yes");
            return 1;
        }
    }
    return 0;
}
