#include "pokem.hpp"

// Class for managing GET/POST request to the local server.
restIO::restIO(std::string server_URL)
{
	this->URL = server_URL;
}

// Fetches all the data from the python server.
std::string restIO::get_all_data()
{
    RestClient::Response all_data = RestClient::get(URL+"/fetch/all");
    if(all_data.code==200)
    {
        return all_data.body;
    }
    else
    {
        std::cout<<"Could not connect to the local server"<<std::endl;
        return "ERROR: "+std::to_string(all_data.code);
    }
}

// Check if the local server is running.
bool restIO::is_connected()
{
    RestClient::Response response = RestClient::get(URL+"/");
    if(response.code==200)
    {
        return true;
    }
    else
    {
        std::cout<<"Could not connect to the local server. "<<"ERROR: "+std::to_string(response.code)<<std::endl;
        return false;
    }
}

// Convert a pokemon struct to the desired json structure.
void restIO::write_entry(pokemon myPoke)
{
    json j_string, j_pokedata;
    j_pokedata["name"] = myPoke.name;
    j_pokedata["types"] = myPoke.types;
    j_pokedata["sprite"] = myPoke.sprite;
    std::string a_string = j_pokedata.dump();
    j_string["data"] = a_string;
    j_string["meta"] = "pokemon";
    std::string as_string = j_string.dump();
    RestClient::Response response = RestClient::post(URL+"/submit", "application/json", as_string);
    if(response.code==201)
    {
        std::cout<<"Added "+myPoke.name+" to the database."<<std::endl;
    }
    else
    {
        std::cout<<"Could not add data entry. ERROR: "<<std::to_string(response.code)<<std::endl;
    }
}

// Class for accessing pokeapi.co.
pokeapi::pokeapi(std::string server_URL)
{
	this->URL = server_URL;
}

// Read the data entry from the pokemon api
std::string pokeapi::get_pokemon(int ID)
{
    std::string my_string = std::to_string(ID);
    RestClient::Response all_data = RestClient::get(URL+my_string);
    if(all_data.code==200)
    {
        return all_data.body;
    }
    else
    {
        std::cout<<"Could not connect to the pokemon api"<<std::endl;
        return "ERROR: "+std::to_string(all_data.code);
    }
}

// Convert a string to a pokemon struct
pokemon pokeapi::create_pokemon(std::string poke_data)
{
    pokemon myPoke;
    auto json_poke = json::parse(poke_data);
    myPoke.name = json_poke["name"];
    myPoke.sprite = json_poke["sprites"]["front_default"];
    int N = json_poke["types"].size();
    for(int i=0;i<N;i++)
    {
        myPoke.types.push_back(json_poke["types"][i]["type"]["name"]);
    }
    return myPoke;
}

// Check if the list contains the pokemon already.
bool poke_found(std::vector<std::string> all_poke, std::string new_poke)
{
    for(std::string current_poke:all_poke)
    {
        if(current_poke == new_poke)
        {
            return false;
        }
    }
    return true;
}
// writes caught pokemons state to file, IF all have been caught
void write_txt(std::string filename, std::string text)
{
    std::fstream txt_file;
    txt_file.open(filename, std::ios::out);
    if(txt_file.is_open())
    {
        txt_file<<text<<std::endl;
    }
}
