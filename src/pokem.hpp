#ifndef Pokelib_LIB_H
#define Pokelib_LIB_H

#include <string>
#include <vector>
#include <random>
#include <fstream>
#include <iostream>
#include "restclient-cpp/restclient.h"
#include <nlohmann/json.hpp>
using json = nlohmann::json;

struct pokemon{
    std::string name;
    std::vector<std::string> types;
    std::string sprite;
};

class restIO
{
public:
	restIO(std::string server_URL);

    std::string get_all_data();
    bool is_connected();
    void write_entry(pokemon myPoke);

private:
	std::string URL;
};

class pokeapi
{
public:
	pokeapi(std::string server_URL);

    std::string get_pokemon(int ID);

    pokemon create_pokemon(std::string poke_data);

private:
	std::string URL;
};

bool poke_found(std::vector<std::string> all_poke, std::string new_poke);
void write_txt(std::string filename, std::string text);
#endif